FROM alpine:3.10

MAINTAINER <rob@buildops.io>

ARG VCS_REF
ARG BUILD_DATE
ARG K8S_VERSION

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/rvanoostrum/cicd-kubectl" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

RUN apk add --update --no-cache ca-certificates \
 && apk add --update --no-cache -t deps curl \
 && curl -L https://storage.googleapis.com/kubernetes-release/release/$K8S_VERSION/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && apk del --purge deps

ENTRYPOINT ["kubectl"]
CMD ["help"]
